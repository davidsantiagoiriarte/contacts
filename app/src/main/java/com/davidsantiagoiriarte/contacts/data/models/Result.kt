package com.davidsantiagoiriarte.contacts.data.models

import com.davidsantiagoiriarte.contacts.domain.models.Contact

data class Result(
    val cell: String,
    val dob: Dob,
    val email: String,
    val gender: String,
    val id: Id,
    val location: Location,
    val login: Login,
    val name: Name,
    val nat: String,
    val phone: String,
    val picture: Picture,
    val registered: Registered
) {
    fun toContact(): Contact {
        return Contact(
            id.value,
            picture.large,
            picture.thumbnail,
            login.username,
            name.first,
            name.last,
            phone,
            email,
            location.coordinates.latitude,
            location.coordinates.longitude
        )
    }
}
