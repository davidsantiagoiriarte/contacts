package com.davidsantiagoiriarte.contacts.data.models

data class APIResponse(
    val info: Info,
    val results: List<Result>
)