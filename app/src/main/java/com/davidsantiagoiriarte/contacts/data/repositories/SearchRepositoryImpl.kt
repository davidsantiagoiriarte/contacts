package com.davidsantiagoiriarte.contacts.data.repositories

import com.davidsantiagoiriarte.contacts.data.database.daos.SearchDao
import com.davidsantiagoiriarte.contacts.data.database.models.DBSearch
import com.davidsantiagoiriarte.contacts.domain.repositories.SearchRepository

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
class SearchRepositoryImpl(private val searchDao: SearchDao) : SearchRepository {
    override suspend fun getRecentSearch(): List<String> {
        return searchDao.getAll().map { it.search }
    }

    override suspend fun addSearch(search: String) {
        searchDao.insertAll(DBSearch(search))
    }

    override suspend fun deleteSearch(search: String) {
        searchDao.delete(DBSearch(search))
    }
}
