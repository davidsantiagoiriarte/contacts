package com.davidsantiagoiriarte.contacts.data.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.davidsantiagoiriarte.contacts.domain.models.Contact

@Entity
data class DBContact(
    @PrimaryKey val id: String,
    val largeImageUrl: String,
    val thumbnailUrl: String,
    val username: String,
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val email: String,
    val latitude: String,
    val longitude: String
) {
    fun toContact(): Contact {
        return Contact(
            id,
            largeImageUrl,
            thumbnailUrl,
            username,
            firstName,
            lastName,
            phoneNumber,
            email,
            latitude,
            longitude
        )
    }
}
