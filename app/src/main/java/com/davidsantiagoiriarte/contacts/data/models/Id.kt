package com.davidsantiagoiriarte.contacts.data.models

data class Id(
    val name: String,
    val value: String
)