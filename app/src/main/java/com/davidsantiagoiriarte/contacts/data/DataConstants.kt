package com.davidsantiagoiriarte.contacts.data

import com.davidsantiagoiriarte.contacts.data.database.models.DBContact
import com.davidsantiagoiriarte.contacts.domain.models.Contact

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */

const val BASE_URL = "https://randomuser.me/api/"
const val RANDOM_USER_SEED = "contacts"
const val DEFAULT_RESULTS = 50

fun Contact.toDBContact(): DBContact {
    return DBContact(
        id,
        largeImageUrl,
        thumbnailUrl,
        username,
        firstName,
        lastName,
        phoneNumber,
        email,
        latitude,
        longitude
    )
}
