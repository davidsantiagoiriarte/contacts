package com.davidsantiagoiriarte.contacts.data.models

data class Registered(
    val age: Int,
    val date: String
)