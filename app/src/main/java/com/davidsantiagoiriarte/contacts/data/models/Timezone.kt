package com.davidsantiagoiriarte.contacts.data.models

data class Timezone(
    val description: String,
    val offset: String
)