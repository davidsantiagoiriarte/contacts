package com.davidsantiagoiriarte.contacts.data.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.davidsantiagoiriarte.contacts.data.database.models.DBContact
import com.davidsantiagoiriarte.contacts.data.database.models.DBSearch

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
@Dao
interface SearchDao {
    @Query("SELECT * FROM dbsearch")
    suspend fun getAll(): List<DBSearch>

    @Insert
    suspend fun insertAll(vararg searchResults: DBSearch)

    @Delete
    suspend fun delete(search: DBSearch)
}
