package com.davidsantiagoiriarte.contacts.data.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.davidsantiagoiriarte.contacts.data.database.models.DBContact

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
@Dao
interface ContactDao {
    @Query("SELECT * FROM dbcontact")
    suspend fun getAll(): List<DBContact>

    @Insert
    suspend fun insertAll(vararg contacts: DBContact)

    @Delete
    suspend fun delete(contact: DBContact)
}
