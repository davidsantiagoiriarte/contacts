package com.davidsantiagoiriarte.contacts.data.models

data class Coordinates(
    val latitude: String,
    val longitude: String
)