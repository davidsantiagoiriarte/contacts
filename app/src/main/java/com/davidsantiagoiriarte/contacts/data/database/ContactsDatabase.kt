package com.davidsantiagoiriarte.contacts.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.davidsantiagoiriarte.contacts.data.database.daos.ContactDao
import com.davidsantiagoiriarte.contacts.data.database.daos.SearchDao
import com.davidsantiagoiriarte.contacts.data.database.models.DBContact
import com.davidsantiagoiriarte.contacts.data.database.models.DBSearch

@Database(entities = [DBContact::class, DBSearch::class], version = 1)
abstract class ContactsDatabase : RoomDatabase() {
    abstract fun contactDato(): ContactDao
    abstract fun searchDao(): SearchDao
}