package com.davidsantiagoiriarte.contacts.data.models

data class Name(
    val first: String,
    val last: String,
    val title: String
)