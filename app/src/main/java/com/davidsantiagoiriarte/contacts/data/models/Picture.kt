package com.davidsantiagoiriarte.contacts.data.models

data class Picture(
    val large: String,
    val medium: String,
    val thumbnail: String
)