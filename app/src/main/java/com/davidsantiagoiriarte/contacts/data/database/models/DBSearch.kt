package com.davidsantiagoiriarte.contacts.data.database.models

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
@Entity
data class DBSearch(@PrimaryKey val search: String)
