package com.davidsantiagoiriarte.contacts.data.network

import com.davidsantiagoiriarte.contacts.data.BASE_URL
import com.davidsantiagoiriarte.contacts.data.models.APIResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
interface RandomUserService {

    @GET("$BASE_URL")
    suspend fun getRandomUsers(
        @Query("page") page: Int,
        @Query("results") results: Int,
        @Query("seed") seed: String
    ): APIResponse
}
