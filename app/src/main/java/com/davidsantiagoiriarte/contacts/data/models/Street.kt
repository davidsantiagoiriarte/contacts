package com.davidsantiagoiriarte.contacts.data.models

data class Street(
    val name: String,
    val number: Int
)