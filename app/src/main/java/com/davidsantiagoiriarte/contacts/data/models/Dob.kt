package com.davidsantiagoiriarte.contacts.data.models

data class Dob(
    val age: Int,
    val date: String
)