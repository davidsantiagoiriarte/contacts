package com.davidsantiagoiriarte.contacts.data.repositories

import com.davidsantiagoiriarte.contacts.data.DEFAULT_RESULTS
import com.davidsantiagoiriarte.contacts.data.RANDOM_USER_SEED
import com.davidsantiagoiriarte.contacts.data.database.daos.ContactDao
import com.davidsantiagoiriarte.contacts.data.network.RandomUserService
import com.davidsantiagoiriarte.contacts.data.toDBContact
import com.davidsantiagoiriarte.contacts.domain.models.Contact
import com.davidsantiagoiriarte.contacts.domain.repositories.ContactsRepository

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
class ContactsRepositoryImpl(
    private val randomUserService: RandomUserService,
    private val contactDao: ContactDao
) :
    ContactsRepository {

    override suspend fun getContacts(page: Int): List<Contact> {
        return randomUserService.getRandomUsers(
            page,
            DEFAULT_RESULTS,
            RANDOM_USER_SEED
        ).results.map {
            it.toContact()
        }
    }

    override suspend fun getSavedContacts(): List<Contact> {
        return contactDao.getAll().map {
            it.toContact()
        }
    }

    override suspend fun addSavedContact(contact: Contact) {
        contactDao.insertAll(contact.toDBContact())
    }

    override suspend fun deleteSavedContact(contact: Contact) {
        contactDao.delete(contact.toDBContact())
    }
}
