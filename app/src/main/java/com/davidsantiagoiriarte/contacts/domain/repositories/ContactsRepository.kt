package com.davidsantiagoiriarte.contacts.domain.repositories

import com.davidsantiagoiriarte.contacts.domain.models.Contact

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
interface ContactsRepository {
    suspend fun getContacts(page: Int): List<Contact>
    suspend fun getSavedContacts(): List<Contact>
    suspend fun addSavedContact(contact: Contact)
    suspend fun deleteSavedContact(contact: Contact)
}
