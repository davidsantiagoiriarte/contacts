package com.davidsantiagoiriarte.contacts.domain.models

data class Contact(
    val id: String,
    val largeImageUrl: String,
    val thumbnailUrl: String,
    val username: String,
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val email: String,
    val latitude: String,
    val longitude: String
)
