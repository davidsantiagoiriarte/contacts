package com.davidsantiagoiriarte.contacts.domain.params

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
data class GetContactsParams(val searchParam: String, val page: Int)
