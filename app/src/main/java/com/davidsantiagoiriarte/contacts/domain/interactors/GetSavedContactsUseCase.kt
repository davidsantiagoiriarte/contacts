package com.davidsantiagoiriarte.contacts.domain.interactors

import com.davidsantiagoiriarte.contacts.base.Interactor
import com.davidsantiagoiriarte.contacts.domain.models.Contact
import com.davidsantiagoiriarte.contacts.domain.repositories.ContactsRepository


/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
class GetSavedContactsUseCase(private val contactsRepository: ContactsRepository) :
    Interactor<List<Contact>, Unit> {
    override suspend fun invoke(params: Unit): List<Contact> {
        return contactsRepository.getSavedContacts()
    }
}