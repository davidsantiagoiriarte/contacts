package com.davidsantiagoiriarte.contacts.domain.interactors

import com.davidsantiagoiriarte.contacts.base.Interactor
import com.davidsantiagoiriarte.contacts.domain.repositories.SearchRepository


/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
class GetRecentSearchesUseCase(private val searchRepository: SearchRepository) :
    Interactor<List<String>, String> {
    override suspend fun invoke(params: String): List<String> {
        return searchRepository.getRecentSearch().filter {
            it.contains(params)
        }
    }
}
