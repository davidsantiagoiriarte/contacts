package com.davidsantiagoiriarte.contacts.domain.interactors

import com.davidsantiagoiriarte.contacts.base.Interactor
import com.davidsantiagoiriarte.contacts.domain.models.Contact
import com.davidsantiagoiriarte.contacts.domain.params.GetContactsParams
import com.davidsantiagoiriarte.contacts.domain.repositories.ContactsRepository

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
class GetContactsUseCase(private val contactsRepository: ContactsRepository) :
    Interactor<List<Contact>, GetContactsParams> {

    override suspend fun invoke(params: GetContactsParams): List<Contact> {
        return contactsRepository.getContacts(params.page).filter {
            params.searchParam.toUpperCase().let { searchParamCapitalized ->
                it.firstName.toUpperCase().contains(searchParamCapitalized) ||
                        it.lastName.toUpperCase().contains(searchParamCapitalized) ||
                        it.username.toUpperCase().contains(searchParamCapitalized)
            }
        }
    }

}
