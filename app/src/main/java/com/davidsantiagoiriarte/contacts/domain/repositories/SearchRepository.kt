package com.davidsantiagoiriarte.contacts.domain.repositories

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
interface SearchRepository {

    suspend fun getRecentSearch(): List<String>
    suspend fun addSearch(search: String)
    suspend fun deleteSearch(search: String)
}
