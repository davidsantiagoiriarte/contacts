package com.davidsantiagoiriarte.contacts.domain.interactors

import com.davidsantiagoiriarte.contacts.base.Interactor
import com.davidsantiagoiriarte.contacts.domain.repositories.SearchRepository

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
class SaveRecentSearchUseCase(
    private val searchRepository: SearchRepository
) :
    Interactor<Unit, String> {

    override suspend fun invoke(params: String) {
        searchRepository.addSearch(params)
    }
}
