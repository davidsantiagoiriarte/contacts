package com.davidsantiagoiriarte.contacts.domain

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */

const val KEY_SAVED_CONTACTS = "KEY_SAVED_CONTACTS"
const val KEY_RECENT_SEARCH_CONTACTS = "KEY_RECENT_SEARCH_CONTACTS"
const val DEFAULT_EMPTY_LIST = "{}"
