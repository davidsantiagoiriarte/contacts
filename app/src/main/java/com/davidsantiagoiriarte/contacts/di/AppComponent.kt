package com.davidsantiagoiriarte.contacts.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(AndroidSupportInjectionModule::class),
        (AppModule::class),
        (RepositoryModule::class),
        (InteractorsModule::class),
        (ServiceModule::class),
        (ActivityBuilder::class),
        (ContextModule::class),
        (DatabaseModule::class)
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {


}
