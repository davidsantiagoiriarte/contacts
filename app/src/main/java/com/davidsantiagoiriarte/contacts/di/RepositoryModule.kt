package com.davidsantiagoiriarte.contacts.di

import com.davidsantiagoiriarte.contacts.data.database.daos.ContactDao
import com.davidsantiagoiriarte.contacts.data.database.daos.SearchDao
import com.davidsantiagoiriarte.contacts.data.network.RandomUserService
import com.davidsantiagoiriarte.contacts.data.repositories.ContactsRepositoryImpl
import com.davidsantiagoiriarte.contacts.data.repositories.SearchRepositoryImpl
import com.davidsantiagoiriarte.contacts.domain.repositories.ContactsRepository
import com.davidsantiagoiriarte.contacts.domain.repositories.SearchRepository
import dagger.Module
import dagger.Provides

/**
 * @author davidiriarte on 25/05/20.
 */
@Module
class RepositoryModule {

    @Provides
    fun provideContactsRepository(
        randomUserService: RandomUserService,
        contactDao: ContactDao
    ): ContactsRepository {
        return ContactsRepositoryImpl(
            randomUserService,
            contactDao
        )
    }

    @Provides
    fun provideSearchRepository(
        searchDao: SearchDao
    ): SearchRepository {
        return SearchRepositoryImpl(
            searchDao
        )
    }

}
