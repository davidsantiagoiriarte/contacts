package com.davidsantiagoiriarte.contacts.di

import com.davidsantiagoiriarte.contacts.base.Interactor
import com.davidsantiagoiriarte.contacts.domain.interactors.*
import com.davidsantiagoiriarte.contacts.domain.models.Contact
import com.davidsantiagoiriarte.contacts.domain.params.GetContactsParams
import com.davidsantiagoiriarte.contacts.domain.repositories.ContactsRepository
import com.davidsantiagoiriarte.contacts.domain.repositories.SearchRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author davidiriarte on 25/05/20.
 */
@Module
class InteractorsModule {

    @Provides
    @Singleton
    fun provideGetContactsUseCase(
        contactsRepository: ContactsRepository
    ): Interactor<List<Contact>, GetContactsParams> {
        return GetContactsUseCase(contactsRepository)
    }

    @Provides
    @Singleton
    fun provideGetRecentSearchesUseCase(
        searchRepository: SearchRepository
    ): Interactor<List<String>, String> {
        return GetRecentSearchesUseCase(searchRepository)
    }

    @Provides
    @Singleton
    fun provideGetSavedContactsUseCase(
        contactsRepository: ContactsRepository
    ): Interactor<List<Contact>, Unit> {
        return GetSavedContactsUseCase(contactsRepository)
    }

    @Provides
    @Singleton
    fun provideSaveContactUseCase(
        contactsRepository: ContactsRepository
    ): Interactor<Unit, Contact> {
        return SaveContactUseCase(contactsRepository)
    }

    @Provides
    @Singleton
    fun provideSaveRecentSearchUseCase(
        searchRepository: SearchRepository
    ): Interactor<Unit, String> {
        return SaveRecentSearchUseCase(searchRepository)
    }

}
