package com.davidsantiagoiriarte.contacts.di

import android.content.Context
import com.davidsantiagoiriarte.contacts.ContactsApplication
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ContextModule(val application: ContactsApplication) {

    @Reusable
    @Provides
    fun provideContext(): Context = application
}