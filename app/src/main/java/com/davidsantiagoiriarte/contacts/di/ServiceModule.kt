package com.davidsantiagoiriarte.contacts.di

import com.davidsantiagoiriarte.contacts.data.BASE_URL
import com.davidsantiagoiriarte.contacts.data.network.RandomUserService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * @author davidiriarte on 25/05/20.
 */
@Module
class ServiceModule {
    private val timeout: Long = 10

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Singleton
    fun provideBaseClient():
            OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient
            .Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideBaseRetrofit(
        baseOkHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(baseOkHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideRandomUserService(retrofit: Retrofit): RandomUserService {
        return retrofit.create(RandomUserService::class.java)
    }

}


