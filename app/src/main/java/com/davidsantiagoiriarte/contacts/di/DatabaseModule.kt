package com.davidsantiagoiriarte.contacts.di

import android.content.Context
import androidx.room.Room
import com.davidsantiagoiriarte.contacts.data.database.ContactsDatabase
import com.davidsantiagoiriarte.contacts.data.database.daos.ContactDao
import com.davidsantiagoiriarte.contacts.data.database.daos.SearchDao
import dagger.Module
import dagger.Provides

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
@Module
class DatabaseModule {

    @Provides
    fun getDatabase(applicationContext: Context): ContactsDatabase {
        return Room.databaseBuilder(
            applicationContext,
            ContactsDatabase::class.java, "contacts"
        ).build()
    }

    @Provides
    fun getContactDao(contactsDatabase: ContactsDatabase): ContactDao {
        return contactsDatabase.contactDato()
    }

    @Provides
    fun getSearchDao(contactsDatabase: ContactsDatabase): SearchDao {
        return contactsDatabase.searchDao()
    }
}
