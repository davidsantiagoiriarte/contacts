package com.davidsantiagoiriarte.contacts

import com.davidsantiagoiriarte.contacts.di.AppComponent
import com.davidsantiagoiriarte.contacts.di.ContextModule
import com.davidsantiagoiriarte.contacts.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */

class ContactsApplication : DaggerApplication() {
    var appComponent: AppComponent? = null

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerAppComponent.builder()
            .contextModule(ContextModule(this))
            .build()
        appComponent = component
        component.inject(this)
        return component
    }
}
