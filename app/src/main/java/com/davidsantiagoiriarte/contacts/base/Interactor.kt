package com.davidsantiagoiriarte.contacts.base

interface Interactor<Response, Params> where Response : Any? {

    suspend operator fun invoke(
        params: Params
    ): Response
}
