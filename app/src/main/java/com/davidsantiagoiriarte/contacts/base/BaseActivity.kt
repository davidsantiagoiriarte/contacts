package com.davidsantiagoiriarte.contacts.base

import dagger.android.support.DaggerAppCompatActivity

open class BaseActivity : DaggerAppCompatActivity() {
}
