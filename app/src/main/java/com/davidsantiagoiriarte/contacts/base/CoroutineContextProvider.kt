package com.davidsantiagoiriarte.contacts.base

import kotlin.coroutines.CoroutineContext


data class CoroutineContextProvider(
    val mainContext: CoroutineContext,
    val backgroundContext: CoroutineContext
)
