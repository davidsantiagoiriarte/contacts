package com.davidsantiagoiriarte.contacts.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch

interface BasePresenter<View : BaseView> : LifecycleObserver {

    var view: View?
    val parentJob: Job
    val coroutineContextProvider: CoroutineContextProvider

    fun bind(view: View) {
        this.view = view
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unBind() {
        view = null
        parentJob.apply {
            cancelChildren()
        }
    }

    fun launchJobOnMainDispatcher(job: suspend CoroutineScope.() -> Unit) {
        CoroutineScope(coroutineContextProvider.mainContext + parentJob).launch {
            job()
        }
    }
}
