package com.davidsantiagoiriarte.contacts.util

import com.google.gson.Gson

/**
 * Created by david on 10/18/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
const val STRING_SEPARATOR = ","

fun Any?.toJson(): String {
    return Gson().toJson(this)
}