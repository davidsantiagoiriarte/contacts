package com.davidsantiagoiriarte.contacts.view.list

import com.davidsantiagoiriarte.contacts.base.BasePresenter
import com.davidsantiagoiriarte.contacts.base.BaseView
import com.davidsantiagoiriarte.contacts.domain.models.Contact

/**
 * Created by david on 10/19/2020.
 * David Iriarte
 * davidsantiagoiriarte@gmail.com
 */
interface ContactListContract {
    interface View : BaseView{
        fun showSavedContacts(contacts : List<Contact>)
        fun showContacts(contacts : List<Contact>)
    }

    interface Presenter : BasePresenter<View>{

    }
}
